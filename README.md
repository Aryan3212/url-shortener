# URL-shortener

This web application will shorten any URL you give to it and also show you the number of times your shortened URL was visited.